package com.hnatiievych;

public class Adress {
    private String city;
    private String street;
    private String bulding;

    public Adress(String city, String street, String bulding) {
        this.city = city;
        this.street = street;
        this.bulding = bulding;
    }

    public String getCity() {
        return city;
    }

    public void setCity(String city) {
        this.city = city;
    }

    public String getStreet() {
        return street;
    }

    public void setStreet(String street) {
        this.street = street;
    }

    public String getBulding() {
        return bulding;
    }

    public void setBulding(String bulding) {
        this.bulding = bulding;
    }
}
