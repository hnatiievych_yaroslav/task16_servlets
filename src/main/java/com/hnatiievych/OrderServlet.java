package com.hnatiievych;

import org.apache.logging.log4j.LogManager;
import org.apache.logging.log4j.Logger;

import javax.servlet.ServletException;
import javax.servlet.http.HttpServlet;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;
import java.io.IOException;
import java.io.PrintWriter;
import java.util.ArrayList;

public class OrderServlet extends HttpServlet {

    private Logger logger = LogManager.getLogger(OrderServlet.class);
    private Order order = new Order();
    private static ArrayList<Pizza> pizzas = new ArrayList<Pizza>();


    @Override
    public void init() throws ServletException {
        pizzas.add(new Pizza("Peperonni"));
        pizzas.add(new Pizza("Cheesee"));
        pizzas.add(new Pizza("Vegei"));
    }

    protected void doGet(HttpServletRequest req, HttpServletResponse resp) throws ServletException, IOException {
        resp.setContentType("text/html");
        PrintWriter out = resp.getWriter();
        out.println("<html><body>");
        out.println("<p>Fill the form for order pizza</p>");
        out.println("<p>______________________________</p>");
        out.println("<form action='order' method='PUT'>" +
                " Name: <input type='text' name='userName'>\n" +
                " <button type='submit'>saveUser</button>"+
                "</form>");

        out.println("<form action='order' method='POST'>" +
                " Pizza name: <select name='pizza'>");
        for (Pizza pizza : pizzas) {
            out.print("<option>" + pizza.getPizzaName() + "</option>");
        }
        out.print("</select>");
        out.println(" Count: <input type='text' name='countOfPizza'>\n" +
                " <button type='submit'>order Pizza</button>" +
                "</form>");

        out.println();
        out.println("<form action='pizzas' method='DELETE'>" +
                "DELETE" +
                "<p> Pizza name: <input type='text' name='pizza_name'>" +
                " <button type='submit'>Delete</button>" +
                "</form>");

        out.println("<p>ORDER LIST</p>");
        out.println("<p>"+order.toString()+"</p>");
//        out.println("<p>"+order.getAdress()+"</p>");
//        out.println("<p>"+order.getListOfPizza()+"</p>");
        out.println("</body></html>");
    }

    @Override
    protected void doPost(HttpServletRequest req, HttpServletResponse resp) throws ServletException, IOException {

        String pizzaName = req.getParameter("pizza");
        int countPizza = Integer.valueOf(req.getParameter("countOfPizza"));
        order.setListOfPizza(new Pizza(pizzaName), countPizza);
        doGet(req, resp);
    }

    @Override
    protected void doPut(HttpServletRequest req, HttpServletResponse resp) throws ServletException, IOException {
        String name = req.getParameter("userName");
        order.setUser(new User(name));
    }

    @Override
    protected void doDelete(HttpServletRequest req, HttpServletResponse resp) throws ServletException, IOException {
        logger.info("Deleting element with id: " + Integer.valueOf(req.getParameter("pizza_id")));
//        pizzaCompany.removePizza(Integer.valueOf(req.getParameter("pizza_id")));
    }


    public void destroy() {
        logger.info("OrderServlet " + this.getServletName() + " has stopped");
    }


}
