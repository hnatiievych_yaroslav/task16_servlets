package com.hnatiievych;

import java.util.Collection;
import java.util.HashMap;
import java.util.Map;
import java.util.Set;

public class Order {
    private User user;
    private Adress adress;
    private Map<Integer,Pizza> listOfPizza;

    public Order() {
    }

    public Order(User user, Adress adress, Map<Integer, Pizza> ListOfPizza) {
        this.user = user;
        this.adress = adress;
        this.listOfPizza = ListOfPizza;
    }

    public User getUser() {
        return user;
    }

    public void setUser(User user) {
        this.user = user;
    }

    public Adress getAdress() {
        return adress;
    }

    public void setAdress(Adress adress) {
        this.adress = adress;
    }

    public Map<Integer, Pizza> getListOfPizza() {
        return listOfPizza;
    }

    public void setListOfPizza(Pizza Pizza, int count) {
        if(listOfPizza==null){
            listOfPizza = new HashMap<>();
            }
       this.listOfPizza.put(count,Pizza);
    }

    @Override
    public String toString() {
        return "Order{" +
                "user=" + user +
                ", adress=" + adress +
                ", listOfPizza=" + listOfPizza.toString() +
                '}';
    }
}

